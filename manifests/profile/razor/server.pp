class site::profile::razor::server(
) inherits ::site::profile::razor::params {

  require ::site::profile::base

  class { '::firewall': ensure => stopped, }

  include ::staging

  File {
    owner => 'nobody',
    group => 'nobody',
    mode => '0655',
  } 

  file { '/srv/tftpboot': ensure => directory, }

  Staging::File { curl_option => '-k', }

  staging::file { 'undionly.kpxe':
    source => 'http://links.puppetlabs.com/pe-razor-ipxe-firmare-3.3',
  } ~>

  file { 'undionly.kpxe':
    path => '/srv/tftpboot/undionly.kpxe',
    ensure => file,
    source => "${::site::params::staging_path}/${module_name}/undionly.kpxe",
  }

  file { 'bootstrap.ipxe':
    path => '/srv/tftpboot/bootstrap.ipxe',
    ensure => file,
    content => epp("${module_name}/profile/razor/server/bootstrap.ipxe.epp",
    { '_ipaddress' => $::ipaddress }),
  }

  class { '::tftp':
    directory => '/srv/tftpboot',
    address => $::ipaddress,
  } 
}
