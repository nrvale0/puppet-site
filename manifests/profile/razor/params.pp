class site::profile::razor::params inherits ::site::params {
  unless 'RedHat' == $::osfamily {
    fail("OS family ${::osfamily} not supported by this profile!")
  }
}
