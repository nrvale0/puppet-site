class site::profile::docker::host(
  $images = {},
  $runs = {}
) inherits ::site::profile::docker::params {

  validate_hash($images, $runs)
  require ::site::profile::docker

  include ::docker
  include ::datadog_agent::integrations::docker

  user { 'dd-agent': ensure => present, groups => ['docker'], }

  $defaults = { 'require' => Class['::docker'] }
  create_resources('::docker::image', $images, $defaults)
  create_resources('::docker::run', $runs, $defaults)
}
