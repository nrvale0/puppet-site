class site::profile::docker::params inherits ::site::params {
  unless 'Ubuntu' == $::operatingsystem {
    fail("OS family ${::osfamily} not supported by this profile!")
  }
}
