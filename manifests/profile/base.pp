class site::profile::base(
  Hash $ssh_authorized_keys                   = {},
  Boolean $loggly_enable                      = true,
  Optional[String] $loggly_token               = undef,
  Boolean $datadog_agent_enable               = true,
  Boolean $install_sysdig                     = true,
  Enum['ruby', 'docker'] $datadog_agent_type  = 'ruby',
  Optional[String] $datadog_api_key           = undef,
  Hash $interactive_users                     = {}
) inherits ::site::params {

  require ::site::profile
  require ::ntp

  file { dirtree($::site::params::staging_path_parent): ensure => directory, } ->
  class { '::staging': path => $::site::params::staging_path, }

  if 'debian' == $::osfamily { require ::apt }

  unless 'linux' != $::kernel and true == $install_sysdig {
    require ::sysdig
    package { ['htop', 'iotop', 'mosh']: ensure => latest, }
  }

  if $datadog_agent_enable {
    if 'ruby' == $datadog_agent_type {
      validate_re($datadog_api_key, '^[a-z0-9]{32}$')
      class { '::datadog_agent':
        api_key            => $datadog_api_key,
        puppet_run_reports => $::pe_server_version ? { /^201.+/ => true, default => undef },
      }
    }

    if 'docker' == $datadog_agent_type {
      include ::docker
      docker::image { 'datadog/docker-dd-agent': ensure => latest, } ~>
      docker::run { 'datadog-agent':
        ensure  => present,
        image   => 'datadog/docker-dd-agent',
        command => template("${module_name}/profile/base/datadog-dd-agent-command.erb"),
        restart => 'on-failure',
        labels  => ['datadog', 'datadog::agent'],
      }
    }
  }

  if $loggly_enable {
    validate_re($loggly_token, '^.+$')
    class { '::loggly::rsyslog': customer_token => $loggly_token, }
  }

  $ssh_authorized_keys_defaults = {}
  create_resources('ssh_authorized_key', $ssh_authorized_keys, $ssh_authorized_keys_defaults)

  $interactive_users_defaults = {}
  create_resources('user', $interactive_users, $interactive_users_defaults)
}
