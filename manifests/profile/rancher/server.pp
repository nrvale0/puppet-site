class site::profile::rancher::server(
) inherits ::site::profile::rancher::params {

  class { '::firewall': ensure => stopped, }

  class { '::site::profile::base': datadog_agent_type => 'docker', }

  include ::docker

  docker::image { ['rancher/server', 'rancher/agent']:
    ensure => 'latest',
  }

  docker::run { 'rancher_admin':
    ensure => present,
    image => 'rancher/server',
    restart => 'on-failure',
    labels => ['rancher', 'rancher::admin'],
    ports => ['8080'],
    subscribe => Docker::Image['rancher/server'],
  }
}
