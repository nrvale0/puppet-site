class site::profile::rancher::params inherits ::site::params {
  unless 'debian' == $::osfamily {
    fail("::osfamily \'${::osfamily}\' not supported!")
  }
}
