class site::profile::rancher::console(
) inherits ::site::profile::rancher::params {

  class { '::firewall': ensure => stopped, }
  class { '::site::profile::base': install_sysdig => false, }
}
