class site::profile::endpoint(

) inherits ::site::profile::endpoint::params {

  require ::site::profile::base

  package { 'python-software-properties': ensure => installed, } ->

  apt::ppa { 'ppa:x2go/stable': ensure => present, } ->

  package { ['x2goserver', 'x2goserver-xsession', 'xfce4'] : 
    ensure => installed,
  }
}
