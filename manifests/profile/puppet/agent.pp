class site::profile::puppet::agent(
  $master = 'puppet',
) inherits ::site::params {

  validate_re($master, '^.+$')

  require ::site::profile::base

  ini_setting { "puppet agent's master":
    ensure => present,
    path => "${::settings::confdir}/puppet.conf}",
    section => 'agent',
    setting => 'server',
    value => $master,
  }
}
