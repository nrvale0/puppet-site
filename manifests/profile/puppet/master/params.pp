class site::profile::puppet::master::params inherits ::site::params {
  $directory_environments_path = "${::settings::confdir}/../code/environments"
  $hiera_config = "${directory_environments_path}/production/hiera.yaml"
}
