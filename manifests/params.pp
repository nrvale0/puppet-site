class site::params {
  case $::osfamily {
    /^(Debian|RedHat)$/ : {}
    default: { fail("Operating system ${::operatingsystem} ${operatingsystemrelease} not supported!") }
  }

  $staging_path_parent = '/var/cache/puppet'
  $staging_path = "${$staging_path_parent}/staging"
}
